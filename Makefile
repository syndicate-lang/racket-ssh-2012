all:
	raco make new-server.rkt

clean:
	find . -name compiled -type d | xargs rm -rf
