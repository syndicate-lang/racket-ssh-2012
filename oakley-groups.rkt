#lang racket/base

;; Construct Oakley MODP Diffie-Hellman groups from RFCs 2409 and 3526.

(provide dh:oakley-group-2
	 dh:oakley-group-14)

;;(require (planet vyzo/crypto))
(require (planet vyzo/crypto/dh))
(require (only-in net/base64 base64-decode))

(define dh:oakley-group-2
  (make-!dh
   1024
   (base64-decode
    #"MIGHAoGBAP//////////yQ/aoiFowjTExmKLgNwc0SkCTgiKZ8x0Agu+pjsTmyJRSgh5jjQE
      3e+VGbPNOkMbMCsKbfJfFDdP4TVtbVHCReSFtXZiXn7G9ExC6aY37WsL/1y29Aa37e44a/ta
      iZ+lrp8kEXxLH+ZJKGZR7OZTgf//////////AgEC")))

(define dh:oakley-group-14
  (make-!dh
   2048
   (base64-decode
    #"MIIBCAKCAQEA///////////JD9qiIWjCNMTGYouA3BzRKQJOCIpnzHQCC76mOxObIlFKCHmO
      NATd75UZs806QxswKwpt8l8UN0/hNW1tUcJF5IW1dmJefsb0TELppjftawv/XLb0Brft7jhr
      +1qJn6WunyQRfEsf5kkoZlHs5Fs9wgB8uKFjvwWY2kg2HFXTmmkWP6j9JM9fg2VdI9yjrZYc
      YvNWIIVSu57VKQdwlpZtZww1Tkq8mATxdGwIyhghfDKQXkYuNs474553LBgOhgObJ4Oi7Aei
      j7XFXfBvTFLJ3ivL9pVYFxg5lUl86pVq5RXSJhiY+gUQFXKOWoqsqmj//////////wIBAg==")))
